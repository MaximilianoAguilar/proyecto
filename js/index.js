$(function(){
    $("[data-toggle = 'tooltip']").tooltip();
    $("[data-toggle = 'popover']").popover();
    $('.carousel').carousel({
      interval: 4000
    });

    $('#contacto').on('show.bs.modal', function(e){
      console.log('Soy el modal y me estoy mostrando');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('show.bs.modal', function(e){
      console.log('Soy el modal y ya me mostré');
    });

    $('#contacto').on('hide.bs.modal', function(e){
      console.log('Soy el modal y me estoy ocultando');
    });

    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('Soy el modal y ya me oculté');
      $('#contactoBtn').prop('disabled', false);
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
    });
  });